# Alphabot2 ROS (1) Simulation

* Based on (or more accurate: copied from) : https://github.com/ssscassio/alphabot2-simulator
* Migrated to ROS noetic on Ubuntu 20.04.
* Migrated to python3
* Working on WSL1

## Building

### File permissions on WSL
Make sure that `/etc/wsl.conf` contains the following section:
```
[automount]
enabled = true
options = "metadata,umask=22,fmask=11"
```

If not - create or update this file.

### Install ROS according to: http://wiki.ros.org/noetic/Installation/Ubuntu
```
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt install curl # if you haven't already installed curl
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
sudo apt update
sudo apt-get install ros-noetic-desktop-full
```
### By default on Ubuntu 20.04 from Windows Store there is no default python selected
```
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.8 1
```
### Build workspace
```
cd catkin_ws
catkin_make
source devel/setup.bash
```

## Running



### Simulation

### On WSL1 there is an issue with Qt5:
```
sudo strip --remove-section=.note.ABI-tag /usr/lib/x86_64-linux-gnu/libQt5Core.so.5
```

### On WSL you will need X server for windows (XMing or vcxsrv)
Install it from: https://sourceforge.net/projects/vcxsrv/

Then configure display on Ubuntu:
```
echo "export DISPLAY=0:0" >> ~/.bashrc
. ~/.bashrc
```

To launch the simulation world run:

```
roslaunch alphabot2_world spawn_world.launch
```

To spawn the robot inside the world run:

```
roslaunch alphabot2_world spawn_robot.launch
```

To launch the pan tilt control node run:

```
roslaunch alphabot2_pantilt alphabot2_pantilt_gazebo.launch
```

## More info : see orginal README.md in : catkin_ws/src/alphabot2-simulator

